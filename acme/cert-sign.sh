#!/bin/bash

#
# Signs the specified domain certificate using letsencrypt.
#

EMAIL=florin.cert@rootshell.ro
DOMAIN=$1
ZONEFILE=$2

# subdomains are ignored (not needed)
[ "x$DOMAIN" == "x" ] && echo "Usage: $0 domain.com /path/to/ZoneFile" && exit
[ ! -f "$ZONEFILE" ] && echo "Usage: $0 domain.com /path/to/ZoneFile" && exit

## change this to something more sensible, e.g. /etc/ssl/schaeffer
: ${CERT_BASE:=/etc/ssl/custom}
: ${ACME_HOME:=/home/acme}

DOMDIR=$CERT_BASE/$DOMAIN

# certbot cannot replace certs, so make sure a new location is used each time (date)
NOW=`date +%Y-%m-%d`

CERT=$DOMDIR/signed/cert-$NOW.crt
FULLCHAIN=$DOMDIR/signed/fullchain-$NOW.pem
CHAIN=$DOMDIR/signed/chain-$NOW.pem
CSR=$DOMDIR/request.csr
#OUT=/tmp/cert-resign.out

if [ -f "$FULLCHAIN" -o -f "$CHAIN" -o -f "$CERT" ]; then
    echo Error, signatures already exist in $DOMDIR:
    echo \ \ $FULLCHAIN
    echo \ \ $CHAIN
    echo \ \ $CERT
    echo You need to remove them first.
    exit 1
fi

echo Signing $DOMAIN

export Nsd_Command="zone-sign.sh $DOMAIN"
export Nsd_ZoneFile="$ZONEFILE"

acme.sh \
    --home ${ACME_HOME} \
    --signcsr --csr $CSR \
    --dns dns_nsd \
    --fullchain-file $FULLCHAIN \
    --ca-file $CHAIN \
    --cert-file $CERT

if [ ! -f $FULLCHAIN -o ! -f $CHAIN -o ! -f $CERT ]; then
	exit 1
fi

#
# Link new certificates into place. The signed.pem file
# will contain the full chain (including the full CA chain).
# For symlinking, need to change dirs.
#
echo Changing to $DOMDIR for symlinking
pushd $DOMDIR
ln -sf ./signed/`basename $CERT`      ./cert.crt
ln -sf ./signed/`basename $FULLCHAIN` ./fullchain.pem
ln -sf ./signed/`basename $CHAIN`     ./chain.pem
popd

# generate certificate in PEM format
openssl x509 -in $CERT -out $DOMDIR/cert.pem -outform PEM
chmod 0640 $DOMDIR/cert.pem

# generate one lighttpd key+pem certificate...
cat $DOMDIR/privkey.pem $DOMDIR/fullchain.pem > $DOMDIR/key+cert.pem
chmod 0640 $DOMDIR/key+cert.pem

echo Certificate signing done, TLSA record is:
ldns-dane -c $CERT create $DOMAIN 443 3 1 1



