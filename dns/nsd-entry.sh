#!/bin/bash

set -euo pipefail

# Where NSD will store its runtime data (caches, PID, zone transfer temp files)
: ${NSD_RUNTIME_DIR:=/var/lib/nsd}
: ${NSD_CONFIG_DIR:=/etc/nsd}

# Set to "yes", "no" or "env"
: ${NSD_GENERATE_KEYS:=yes}

NSD_CONF=${NSD_CONFIG_DIR}/nsd.conf

if [ -f "${NSD_CONF}.template" ]; then
    echo "Substituting nsd-conf"
    cat "$NSD_CONF".template | envsubst > "${NSD_CONF}"
fi

if [ -f "${NSD_CONF}.gen" ]; then
    echo "Generating nsd-conf"
    bash -f "$NSD_CONF".gen > "${NSD_CONF}"
fi

if [ "$NSD_GENERATE_KEYS" == "yes" ]; then
    # This will overwrite nsd_control.* and nsd_server.*
    echo "Generating nsd-control keys from scratch"
    nsd-control-setup -d "${NSD_CONFIG_DIR}"
elif [ "$NSD_GENERATE_KEYS" == "env" ]; then
    echo "Substituting nsd-control keys from env vars"
    for f in ${NSD_CONFIG_DIR}/*.key.template \
	     ${NSD_CONFIG_DIR}/*.pem.template; do
	echo "$f" | envsubst > "${f%%.template}"
    done
    for f in ${NSD_CONFIG_DIR}/*.key.gen \
	     ${NSD_CONFIG_DIR}/*.pem.gen; do
	bash -f "$f" > "${f%%.gen}"
    done
fi

chown nsd:nsd $NSD_RUNTIME_DIR -R

# Necessary directories
mkdir -p \
      ${NSD_RUNTIME_DIR} \
      ${NSD_RUNTIME_DIR}/pid \
      ${NSD_RUNTIME_DIR}/db \
      ${NSD_RUNTIME_DIR}/tmp

chown nsd:nsd ${NSD_RUNTIME_DIR} -R

nsd -d -c "${NSD_CONFIG_DIR}/nsd.conf"
