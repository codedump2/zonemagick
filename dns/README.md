NSD image for ZoneMagick
========================

Variables:

 - `NSD_RUNTIME_DIR`: the directory (inside the container) where NSD
   will keep its runtime data. Defaults to `/var/lib/nsd`.
   
 - `NSD_CONFIG_FILE`: path of `nsd.conf` (defaults to
   `/etc/nsd/nsd.conf`).
