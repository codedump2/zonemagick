ZoneMagick
==========

This is a collection of interacting containers for demployment of
a DHCP and DNS server, with associated Acme DNS-01 Let's Encrypt
certificate generation.


The moving parts
----------------

The following main services are deployed (some are optional):

 - [NSD](https://nsd.docs.nlnetlabs.nl/en/latest/) -- an small & sweet
   authoritative DNS server with focus on "speed, reliability, stability
   and security". You want this because, like BIND, this is one of the
   reference implementations of the DNS standward. It's also used by
   a large part of the world's root DNS servers, so obviously it's
   robust. And you don't like BIND of any color, so this is really the
   only other obvious option...
   
 - [KEA](https://www.isc.org/kea/) -- the modern ISC DHCP server; the
   deployment includes a MariaDB container to act as a host database
   for KEA. You want this because "this is what cool people use"...
   or something.
 
 - [acme.sh](https://github.com/acmesh-official/acme.sh) -- an automated
   [Let's Encrypt](https://letsencrypt.org/) certificate renewal bot; 
   unlike the NSD and KEA service, which need to run continuously, this
   one is triggered periodically (weekly?) for renewal only. 
   
   
The main idea is that KEA serves your site's (local-network) DHCP requests
and keeps a database of all leases. You want these in your DNS server --
and here is wher it gets complicated: KEA *could* talk to your DNS server
and update domains using a dyndns protocol, but... NSD doesn't support any.
Your best bet is to patch the zonefiles on the fly. And while you're at it,
this is how you also implement DNS-01 challenges of Let's Encrypt -- by
patching zonefiles on the fly.


The data
--------

To do this, you store your DNS and DHCP configurations in specific
git repositories:

 - `DHCP_CONF_REPO`: holds KEA dhcp configuration files or
   *templates* (i.e. text files ending on `.template`, which will
   be substituted for environment variables prior to use)
   
 - `DNS_CONF_REPO`: holds NSD configuration files or templates,
   as well as all your zonefiles (or templates).

These repos can (and are encouraged to) be public ones, you're not going
to put any secrets inside the config files). Instead, you're going to use
specific environment variables in files which are called like your original
config files (e.g. `nsd.conf`), but ending in `.template` instead
(e.g. `nsd.conf.template`).


The process
-----------

A number of auxiliary services are deployed, in addition to the above:

 - **dhcp-to-dns** -- a shell script that (optionally) connects to the
   KEA MariaDB server, retrieves a list of hosts, and updates the
   zonefile configuration with current IP values; this is triggered
   by the KEA DHCP container (see above) every time a new lease
   is issued.
   
 - **mk-dhcp-conf** -- a service which creates a DHCP config container
   and uploads it to a (possibly public) registry of your choice. This
   is triggered by any commit to the DHCP config repo.
   
 - **mk-dns-conf** -- a service which creates a DNS config container and
   uploads it to a (possibly public) registry of your choice.
   This is triggered by any commit to the DNS config repo.
   
 - **dns-conf** -- a data-only container with the NSD configuration.
 
 - **dhcp-conf** -- a data-only container with the KEA configuration.

Across the whole lifecycle there is no static data (i.e. repositories,
configuration files or container images) that need to contain secrets.
All secrets are kept on the host only (e.g. using `podman secret`)
and are passed to the running containers as environment variables.


The variables
-------------

Environment variables controlling the ZoneMagick setup are in a bit
of a state of flow: there is no "ultimate collection of allowed
variables" *per se*. All variables that are written in `.template`
files and properly passed on to `podman` on the host will have the
intended effect: their contents will end up in the corresponding
config file.

There are, however, *some* variables which have a specific effect
on the system as a whole, and other variables which are highly
recommended. For example see the following files:

 - [Rsh's KEA config](https://gitlab.com/codedump2/rsh-conf-dhcp/)
 
 - [Rsh's NSD config](https://gitlab.com/codedump2/rsh-conf-zonefiles/)
 
Additionally, some ZoneMagick tools generate variables of their own
which can be used in the config files
(e.g. [dhcp-to-dns](dhcp-to-dns/README.md)).


The deployment
--------------

To use the ZoneMagick setup for your own site, the deployment
is 3-fold:
 
 - ZoneMagick infrastructure deployment (i.e. *this* project
   on a git server -- preferrably GitLab, because we already have
   the corresponding CI-file included) designe to create the
   necessary images (`zma-dns`, `zma-dhcp`, `zma-dns2dhcp`,
   `zma-mk-dns-conf`, `zma-mk-dhcp-conf`).
   This is done simply by cloning this repository, or uploading
   a copy of it, to your GitLab account (or similar). The included
   CI YAML file should take care of the rest. (You are well advised
   to go through the YAML file and watch out for necessary
   configuration env-vars to be passed to the gitlab CI).
 
 - Site-specific configuration repositories (DNS and DHCP, see above).
   All you need to do is create those two repos and update the 
   corresponding URLs in the ZoneMagick CI/CD YAML file.
 
 - Deployment of the ZoneMagick pod(s) / containers on the site-specific
   host. Use the included Podman Compose file, and/or export your
   pods as systemd units to be started / restarted upon reboot.


Bugs & Caveats
--------------

...try not to snap your fingers?... -_-
