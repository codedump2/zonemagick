ISC KEA-DHCPD Image
===================

Uses the following variables:

 - `KEA_CONFIG_FILE`: configuration file of the KEA DHCP server.
   Defaults to `/etc/kea/kea-dhcpd.conf`. If the image has a template
   file present (i.e. `${KEA_CONFIG_FILE}.template`), then it is env-var
   substituted first, in order to generate `$KEA_CONFIG_FILE`.
   
 - `KEA_DB_HOST`: host name of the MariaDB host for KEA
 
 - `KEA_DB_PASS`: password for the MariaDB connection. If both `KEA_DB_HOST`
   and `KEA_DB_PASS` are specified, before starting KEA, the database
   is initialized.
