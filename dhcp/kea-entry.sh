#!/bin/bash

set -euo pipefail

# Config file of the KEA DHCP server.
# This is the name of the "true" config file, not of the template.
: ${KEA_CONFIG_FILE:=/etc/kea/kea-dhcpd.conf}

: ${KEA_DB_USER:=kea}

: ${KEA_DB_NAME:=kea}

# kea-dhcpd.conf template requires KEA_DB_PASS and KEA_DB_HOST env vars
if [ -f "$KEA_CONFIG_FILE".template ]; then
    cat "$KEA_CONFIG_FILE".template | envsubst > "$KEA_CONFIG_FILE"
fi

if [ -f "$KEA_CONFIG_FILE".gen ]; then
    bash -f "$KEA_CONFIG_FILE".gen > "$KEA_CONFIG_FILE"
fi

# If we have DB env vars, we try to initialize the database.
# Otherwise we just magically assume the DB exists and the
# config file has all the details...
if [ ! -z "$KEA_DB_HOST" -a ! -z "$KEA_DB_PASS" ]; then
    kea-admin db-init mysql -u $KEA_DB_USER -p $KEA_DB_PASS -n $KEA_DB_NAME -h $KEA_DB_HOST || :
fi

kea-dhcp4 -c "$KEA_CONFIG_FILE"
